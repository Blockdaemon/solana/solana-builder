#!/usr/bin/env bash
#
here="$(dirname "$0")"
readlink_cmd="readlink"
echo "OSTYPE IS: $OSTYPE"
if [[ $OSTYPE == darwin* ]]; then
  # Mac OS X's version of `readlink` does not support the -f option,
  # But `greadlink` does, which you can get with `brew install coreutils`
  readlink_cmd="greadlink"

  if ! command -v ${readlink_cmd} &> /dev/null
  then
    echo "${readlink_cmd} could not be found. You may need to install coreutils: \`brew install coreutils\`"
    exit 1
  fi
fi


cargo="$("${readlink_cmd}" -f "${here}/solana/cargo")"
installDir=.
buildVariant=release
releaseFlag=--release

echo "Install location: $installDir ($buildVariant)"

installDir="$(mkdir -p "$installDir"; cd "$installDir"; pwd)"
mkdir -p "$installDir/bin/deps"

BINS=(
    solana
   # solana-bench-tps
   # solana-faucet
   solana-gossip
   # solana-install
    solana-keygen
    solana-ledger-tool
   # solana-log-analyzer
   # solana-net-shaper
    solana-validator
   # rbpf-cli
)

binArgs=()
for bin in "${BINS[@]}"; do
  binArgs+=(--bin "$bin")
done

SECONDS=0

mkdir -p "$installDir/bin"

$cargo build $releaseFlag "${binArgs[@]}"

for bin in "${BINS[@]}"; do
  mv  "target/$buildVariant/$bin" "$installDir"/bin
done

echo "Done after $SECONDS seconds"
